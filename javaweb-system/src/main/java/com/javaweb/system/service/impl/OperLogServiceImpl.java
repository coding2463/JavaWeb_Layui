// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.OperLogConstant;
import com.javaweb.system.entity.OperLog;
import com.javaweb.system.mapper.OperLogMapper;
import com.javaweb.system.query.OperLogQuery;
import com.javaweb.system.service.IOperLogService;
import com.javaweb.system.vo.operlog.OperLogListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-05-04
 */
@Service
public class OperLogServiceImpl extends BaseServiceImpl<OperLogMapper, OperLog> implements IOperLogService {

    @Autowired
    private OperLogMapper operLogMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        OperLogQuery operLogQuery = (OperLogQuery) query;
        // 查询条件
        QueryWrapper<OperLog> queryWrapper = new QueryWrapper<>();
        // 模块标题
        if (!StringUtils.isEmpty(operLogQuery.getTitle())) {
            queryWrapper.like("title", operLogQuery.getTitle());
        }
        // 业务类型：0其它 1新增 2修改 3删除
        if (operLogQuery.getBusinessType() != null && operLogQuery.getBusinessType() > 0) {
            queryWrapper.eq("business_type", operLogQuery.getBusinessType());
        }
        // 操作类别：0其它 1后台用户 2手机端用户
        if (operLogQuery.getOperatorType() != null && operLogQuery.getOperatorType() > 0) {
            queryWrapper.eq("operator_type", operLogQuery.getOperatorType());
        }
        // 操作状态：1正常 2异常
        if (operLogQuery.getStatus() != null && operLogQuery.getStatus() > 0) {
            queryWrapper.eq("status", operLogQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<OperLog> page = new Page<>(operLogQuery.getPage(), operLogQuery.getLimit());
        IPage<OperLog> data = operLogMapper.selectPage(page, queryWrapper);
        List<OperLog> operLogList = data.getRecords();
        List<OperLogListVo> operLogListVoList = new ArrayList<>();
        if (!operLogList.isEmpty()) {
            operLogList.forEach(item -> {
                OperLogListVo operLogListVo = new OperLogListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, operLogListVo);
                // 业务类型描述
                if (operLogListVo.getBusinessType() != null && operLogListVo.getBusinessType() > 0) {
                    operLogListVo.setBusinessTypeName(OperLogConstant.OPERLOG_BUSINESSTYPE_LIST.get(operLogListVo.getBusinessType()));
                }
                // 操作类别描述
                if (operLogListVo.getOperatorType() != null && operLogListVo.getOperatorType() > 0) {
                    operLogListVo.setOperatorTypeName(OperLogConstant.OPERLOG_OPERATORTYPE_LIST.get(operLogListVo.getOperatorType()));
                }
                // 操作状态描述
                if (operLogListVo.getStatus() != null && operLogListVo.getStatus() > 0) {
                    operLogListVo.setStatusName(OperLogConstant.OPERLOG_STATUS_LIST.get(operLogListVo.getStatus()));
                }
                operLogListVoList.add(operLogListVo);
            });
        }
        return JsonResult.success("操作成功", operLogListVoList, data.getTotal());
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        OperLog entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

    /**
     * 创建系统操作日志
     *
     * @param operLog 操作日志对象
     */
    @Override
    public void insertOperlog(OperLog operLog) {
        operLogMapper.insertOperlog(operLog);
    }

}