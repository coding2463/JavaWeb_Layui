// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.ConfigData;
import com.javaweb.system.mapper.ConfigDataMapper;
import com.javaweb.system.service.IConfigWebService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ConfigWebServiceImpl implements IConfigWebService {

    @Autowired
    private ConfigDataMapper configMapper;

    /**
     * @param map 参数
     * @return
     */
    @Override
    public JsonResult configEdit(Map<String, Object> map) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue().toString();
            System.out.println("KEY:" + key + ",值：" + value);

            if (key.contains("checkbox")) {
                // 复选框
                String[] item = key.split("__");
                key = item[0];
            } else if (key.contains("upimage")) {
                // 单图上传
                String[] item = key.split("__");
                key = item[0];
                if (value.contains(CommonConfig.imageURL)) {
                    value = value.replaceAll(CommonConfig.imageURL, "");
                }
            } else if (key.contains("upimgs")) {
                // 多图上传
                String[] item = key.split("__");
                key = item[0];

                String[] stringsVal = value.split(",");
                List<String> stringList = new ArrayList<>();
                for (String s : stringsVal) {
                    if (s.contains(CommonConfig.imageURL)) {
                        stringList.add(s.replaceAll(CommonConfig.imageURL, ""));
                    } else {
                        // 已上传图片
                        stringList.add(s.replaceAll(CommonConfig.imageURL, ""));
                    }
                }
                value = StringUtils.join(stringList, ",");
            } else if (key.contains("ueditor")) {
                String[] item = key.split("__");
                key = item[0];
                // 处理富文本信息(预留扩展接口，暂未开放)

            }
            // 更新信息
            QueryWrapper<ConfigData> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("code", key);
            ConfigData config = configMapper.selectOne(queryWrapper);
            if (config == null) {
                continue;
            }
            config.setValue(value);
            configMapper.updateById(config);
        }
        return JsonResult.success();
    }
}
