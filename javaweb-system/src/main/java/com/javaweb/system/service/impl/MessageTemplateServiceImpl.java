// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.MessageTemplateConstant;
import com.javaweb.system.entity.MessageTemplate;
import com.javaweb.system.mapper.MessageTemplateMapper;
import com.javaweb.system.query.MessageTemplateQuery;
import com.javaweb.system.service.IMessageTemplateService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.system.vo.message.MessageTemplateListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 消息模板 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-05-04
 */
@Service
public class MessageTemplateServiceImpl extends BaseServiceImpl<MessageTemplateMapper, MessageTemplate> implements IMessageTemplateService {

    @Autowired
    private MessageTemplateMapper messageTemplateMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        MessageTemplateQuery messageTemplateQuery = (MessageTemplateQuery) query;
        // 查询条件
        QueryWrapper<MessageTemplate> queryWrapper = new QueryWrapper<>();
        // 模板标题
        if (!StringUtils.isEmpty(messageTemplateQuery.getTitle())) {
            queryWrapper.like("title", messageTemplateQuery.getTitle());
        }
        // 模板类型：1系统模板 2短信模板 3邮件模板 4微信模板 5推送模板
        if (messageTemplateQuery.getType() != null && messageTemplateQuery.getType() > 0) {
            queryWrapper.eq("type", messageTemplateQuery.getType());
        }
        // 状态：1在用 2停用
        if (messageTemplateQuery.getStatus() != null && messageTemplateQuery.getStatus() > 0) {
            queryWrapper.eq("status", messageTemplateQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("id");

        // 查询数据
        IPage<MessageTemplate> page = new Page<>(messageTemplateQuery.getPage(), messageTemplateQuery.getLimit());
        IPage<MessageTemplate> data = messageTemplateMapper.selectPage(page, queryWrapper);
        List<MessageTemplate> messageTemplateList = data.getRecords();
        List<MessageTemplateListVo> messageTemplateListVoList = new ArrayList<>();
        if (!messageTemplateList.isEmpty()) {
            messageTemplateList.forEach(item -> {
                MessageTemplateListVo messageTemplateListVo = new MessageTemplateListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, messageTemplateListVo);
                // 模板类型描述
                if (messageTemplateListVo.getType() != null && messageTemplateListVo.getType() > 0) {
                    messageTemplateListVo.setTypeName(MessageTemplateConstant.MESSAGETEMPLATE_TYPE_LIST.get(messageTemplateListVo.getType()));
                }
                // 状态描述
                if (messageTemplateListVo.getStatus() != null && messageTemplateListVo.getStatus() > 0) {
                    messageTemplateListVo.setStatusName(MessageTemplateConstant.MESSAGETEMPLATE_STATUS_LIST.get(messageTemplateListVo.getStatus()));
                }
                // 添加人名称
                if (messageTemplateListVo.getCreateUser() > 0) {
                    messageTemplateListVo.setCreateUserName(UserUtils.getName((messageTemplateListVo.getCreateUser())));
                }
                // 更新人名称
                if (messageTemplateListVo.getUpdateUser() > 0) {
                    messageTemplateListVo.setUpdateUserName(UserUtils.getName((messageTemplateListVo.getUpdateUser())));
                }
                messageTemplateListVoList.add(messageTemplateListVo);
            });
        }
        return JsonResult.success("操作成功", messageTemplateListVoList, data.getTotal());
    }

    /**
     * 获取记录详情
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        MessageTemplate entity = (MessageTemplate) super.getInfo(id);
        return entity;
    }

    /**
     * 添加或编辑记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(MessageTemplate entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        MessageTemplate entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(MessageTemplate entity) {
        if (entity.getId() == null || entity.getId() <= 0) {
            return JsonResult.error("记录ID不能为空");
        }
        if (entity.getStatus() == null) {
            return JsonResult.error("记录状态不能为空");
        }
        return super.setStatus(entity);
    }
}