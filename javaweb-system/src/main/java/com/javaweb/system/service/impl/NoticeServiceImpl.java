// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.NoticeConstant;
import com.javaweb.system.entity.Notice;
import com.javaweb.system.mapper.NoticeMapper;
import com.javaweb.system.query.NoticeQuery;
import com.javaweb.system.service.INoticeService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.system.vo.notice.NoticeListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 通知公告 服务实现类
 * </p>
 *
 * @author 鲲鹏
  * @date 2020-05-04
 */
@Service
public class NoticeServiceImpl extends BaseServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        NoticeQuery noticeQuery = (NoticeQuery) query;
        // 查询条件
        QueryWrapper<Notice> queryWrapper = new QueryWrapper<>();
        // 通知标题
        if (!StringUtils.isEmpty(noticeQuery.getTitle())) {
            queryWrapper.like("title", noticeQuery.getTitle());
        }
        // 通知来源：1云平台
        if (noticeQuery.getSource() != null && noticeQuery.getSource() > 0) {
            queryWrapper.eq("source", noticeQuery.getSource());
        }
        // 是否置顶：1已置顶 2未置顶
        if (noticeQuery.getIsTop() != null && noticeQuery.getIsTop() > 0) {
            queryWrapper.eq("is_top", noticeQuery.getIsTop());
        }
        // 发布状态：1草稿箱 2立即发布 3定时发布
        if (noticeQuery.getStatus() != null && noticeQuery.getStatus() > 0) {
            queryWrapper.eq("status", noticeQuery.getStatus());
        }
        // 推送状态：1已推送 2未推送
        if (noticeQuery.getIsSend() != null && noticeQuery.getIsSend() > 0) {
            queryWrapper.eq("is_send", noticeQuery.getIsSend());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<Notice> page = new Page<>(noticeQuery.getPage(), noticeQuery.getLimit());
        IPage<Notice> data = noticeMapper.selectPage(page, queryWrapper);
        List<Notice> noticeList = data.getRecords();
        List<NoticeListVo> noticeListVoList = new ArrayList<>();
        if (!noticeList.isEmpty()) {
            noticeList.forEach(item -> {
                NoticeListVo noticeListVo = new NoticeListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, noticeListVo);
                // 通知来源描述
                if (noticeListVo.getSource() != null && noticeListVo.getSource() > 0) {
                    noticeListVo.setSourceName(NoticeConstant.NOTICE_SOURCE_LIST.get(noticeListVo.getSource()));
                }
                // 是否置顶描述
                if (noticeListVo.getIsTop() != null && noticeListVo.getIsTop() > 0) {
                    noticeListVo.setIsTopName(NoticeConstant.NOTICE_ISTOP_LIST.get(noticeListVo.getIsTop()));
                }
                // 发布状态描述
                if (noticeListVo.getStatus() != null && noticeListVo.getStatus() > 0) {
                    noticeListVo.setStatusName(NoticeConstant.NOTICE_STATUS_LIST.get(noticeListVo.getStatus()));
                }
                // 推送状态描述
                if (noticeListVo.getIsSend() != null && noticeListVo.getIsSend() > 0) {
                    noticeListVo.setIsSendName(NoticeConstant.NOTICE_ISSEND_LIST.get(noticeListVo.getIsSend()));
                }
                // 添加人名称
                if (noticeListVo.getCreateUser() > 0) {
                    noticeListVo.setCreateUserName(UserUtils.getName((noticeListVo.getCreateUser())));
                }
                // 更新人名称
                if (noticeListVo.getUpdateUser() > 0) {
                    noticeListVo.setUpdateUserName(UserUtils.getName((noticeListVo.getUpdateUser())));
                }
                noticeListVoList.add(noticeListVo);
            });
        }
        return JsonResult.success("操作成功", noticeListVoList, data.getTotal());
    }

    /**
     * 获取记录详情
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Notice entity = (Notice) super.getInfo(id);
        return entity;
    }

    /**
     * 添加或编辑记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Notice entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        Notice entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(Notice entity) {
        if (entity.getId() == null || entity.getId() <= 0) {
            return JsonResult.error("记录ID不能为空");
        }
        if (entity.getStatus() == null) {
            return JsonResult.error("记录状态不能为空");
        }
        return super.setStatus(entity);
    }
}